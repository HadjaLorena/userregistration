using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UserRegistration.Models;
using UserRegistration.Utils;

namespace UserRegistration.DAO;

public class Database
{
    private static Database instance;
    private readonly ObservableCollection<User> users = [];
    
    public static Database CreateInstance() {
        if (instance == null)
        {
            instance = new Database();
        } 
        return instance;
    }

    public void SaveUser(User user) {
        IsValid(user);
        users.Add(user);
    }

    private void IsValid(User user)
    {
        string ErrorMessage = "";
        
        if (!IsEmail(user.Email))
        {
            ErrorMessage = "E-mail inválido!";
            CreateValidationError(ErrorMessage);
        }

        if (!IsName((user.Name)))
        {
            ErrorMessage = "Nome inválido!";
            CreateValidationError(ErrorMessage);
        }

        if (!IsName(user.Surname))
        {
            ErrorMessage = "Sobrenome inválido!";
            CreateValidationError(ErrorMessage);
        }
    }

    private static void CreateValidationError(string errorMessage)
    {
        ErrorStack.PushError(errorMessage);
        throw new CustomizedErrors.InvalidParameterError(errorMessage);
    }

    private bool IsEmail(string email)
    {
        return Validator.ValidateEmail(email);
    }

    private bool IsName(string name)
    {
        return Validator.ValidateName(name);
    }

    public List<User> GetUserList()
    {
        return users.ToList();
    }

    public bool UserListIsEmpty()
    {
        if (users.ToList().Count == 0)
        {
            return true;
        }

        return false;
    }
}