﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UserRegistration.DAO;
using UserRegistration.Helpers.Observable;
using UserRegistration.Models;

namespace UserRegistration.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    public static ObservableCollection<User> Users { get; set;} = [];
    private static Database _database;

    public static void InitApp()
    {
       ConnectToDatabase();
       IsConnected();
    }

    private static void ConnectToDatabase()
    {
        _database = Database.CreateInstance();
    }

    private static void IsConnected()
    {
        string ErrorMessage = "";
        
        if (_database == null)
        {
            ErrorMessage = "Não foi possível estabelecer a conexão com o banco de dados!";
            ErrorStack.PushError(ErrorMessage);
            throw new CustomizedErrors.DataBaseConnectionError(ErrorMessage);
        }
        Console.WriteLine("Conexão com o banco de dados feita com sucesso!");
    }
    
    public static void AddNewUser(User user)
    {
        _database.SaveUser(user);
        Users.Add(user);
        
        MessageObserver messageObserver = new();
        Subject subject = new();
        subject.Add(messageObserver);
        subject.Notify($"Usuário {user.Email} adicionado com sucesso!");

    }
    
    public static void FilterUser(string textToFilter) {
        string textWithoutSpace = textToFilter.Trim();
        if (IsNullOrEmptyText(textWithoutSpace)) return;
        CheckIfNotHaveUserToFilter();
        
        MessageObserver messageObserver = new();
        Subject subject = new();
        subject.Add(messageObserver);
        subject.Notify("Filtrando");

        List<User> filteredUsers = Users.Where(obj => obj.Email.Contains(textWithoutSpace)).ToList();

        Users.Clear();
        ImplementFilteredUsers(filteredUsers);
    }

    public static bool IsNullOrEmptyText(string text) {
        if (string.IsNullOrEmpty(text)) {
            Users.Clear();
            foreach (User user in _database.GetUserList()) {
                Users.Add(user);
            }

            return true;
        }

        return false;
    }

    public static void CheckIfNotHaveUserToFilter() {
        if (Users.Count == 0) {
            foreach (User user in _database.GetUserList()) {
                Users.Add(user);
            }
        }
    }

    public static void ImplementFilteredUsers(List<User> filteredUsers) {
        foreach (User user in filteredUsers) {
            Users.Add(user);
        }
    }
}