using System;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using UserRegistration.Helpers.Observable;
using UserRegistration.Models;
using UserRegistration.ViewModels;

namespace UserRegistration.Views;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();

        try
        {
            MainWindowViewModel.InitApp();
        }
        catch (Exception nameError)
        {
            HandleError(ErrorStack.PopError(), nameError);
        }
    }

    public void AddUserButton(object sender, RoutedEventArgs e)
    {
        try {
            string nameText = name.Text;
            string surnameText = surname.Text;
            string emailText = email.Text;

            if (!IsEveryFieldFilled(nameText, surnameText, emailText))
            {
                ShowWarningMessage("Existem campos não preenchidos!");
                return;   
            }

            User user = new(nameText, surnameText, emailText);
            MainWindowViewModel.AddNewUser(user);
            ShowSuccessfulMessage($"Usuário {user.Name} cadastrado com sucesso!");
            
            Clear();
        } catch (Exception nameError) { 
            HandleError(ErrorStack.PopError(), nameError);
        }
    }

    private void ShowSuccessfulMessage(string successfulMessage)
    {
        successLabel.Text = successfulMessage;
        successLabel.IsVisible = true;
        errorLabel.IsVisible = false;
        warningLabel.IsVisible = false;
    }

    private void ShowWarningMessage(string warningMessage)
    {
        warningLabel.Text = warningMessage;
        warningLabel.IsVisible = true;
        successLabel.IsVisible = false;
        errorLabel.IsVisible = false;
    }
    
    private void HandleError(string errorMessage, Exception exception) {
        errorLabel.Text = errorMessage;
        errorLabel.IsVisible = true;
        successLabel.IsVisible = false;
        warningLabel.IsVisible = false;
    }
    
    private bool IsEveryFieldFilled(string nameText, string surnameText, string emailText)
    {
        return !string.IsNullOrEmpty(nameText) && !string.IsNullOrEmpty(surnameText) && !string.IsNullOrEmpty(emailText);
    }
    
    private void Clear() {
        name.Text = "";
        email.Text = "";
        surname.Text = "";
    }
    
    private void FilterTextUpdate(object sender, KeyEventArgs e) {
        MainWindowViewModel.FilterUser(filterByEmail.Text);
    }

    private void DataGrid_OnSorting(object? sender, DataGridColumnEventArgs e)
    {
        MessageObserver messageObserver = new();
        Subject subject = new();
        subject.Add(messageObserver);
        subject.Notify("Ordenando");
    }
}